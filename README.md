# wackamole.houseofcards

A solution for the 'house of cards' problem, #10.

# Running this application.

This is a Maven Java project. Simply enter the following at the command-line:

```
mvn exec:java
```

# Results of running application.

Based on the specs of the problem, first two are the examples, last three are the question...

```
Running application for: [39] packs..
Size of deck before pulling cards: 2028
Size of deck after pulling cards: 1
8D2009

Running application for: [18] packs..
Size of deck before pulling cards: 936
Size of deck after pulling cards: 1
JH849

Running application for: [1] packs..
Size of deck before pulling cards: 52
Size of deck after pulling cards: 1
KC41

Running application for: [39] packs..
Size of deck before pulling cards: 2028
Size of deck after pulling cards: 1
8D2009

Running application for: [100000] packs..
Size of deck before pulling cards: 5200000
Size of deck after pulling cards: 1
8D2011393
```

# Worked Example

Found this in my scribbled notes when attempting to understand if the printed examples on the handout were wrong. 

Setup: 1 pack, with only 5,4,3,2 from each suit (to reduce no. of permutations).

```
S5-S4-S3-S2-H5-H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2
S4-S3-S2-H5-H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5
S3-S2-H5-H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5
S2-H5-H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5-S3   
H5-H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5-S3   
H4-H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5-S3-H5
H3-H2-D5-D4-D3-D2-C5-C4-C3-C2-S5-S3-H5  
H2-D5-D4-D3-D2-C5-C4-C3-C2-S5-S3-H5-H3
D5-D4-D3-D2-C5-C4-C3-C2-S5-S3-H5-H3
D4-D3-D2-C5-C4-C3-C2-S5-S3-H5-H3-D5
D3-D2-C5-C4-C3-C2-S5-S3-H5-H3-D5
D2-C5-C4-C3-C2-S5-S3-H5-H3-D5-D3
C5-C4-C3-C2-S5-S3-H5-H3-D5-D3
C4-C3-C2-S5-S3-H5-H3-D5-D3-C5
C3-C2-S5-S3-H5-H3-D5-D3-C5
C2-S5-S3-H5-H3-D5-D3-C5-C3
S5-S3-H5-H3-D5-D3-C5-C3
S3-H5-H3-D5-D3-C5-C3-S5
H5-H3-D5-D3-C5-C3-S5
H3-D5-D3-C5-C3-S5-H5
D5-D3-C5-C3-S5-H5
D3-C5-C3-S5-H5-D5
C5-C3-S5-H5-D5
C3-S5-H5-D5-C5
S5-H5-D5-C5
H5-D5-C5-S5
D5-C5-S5
C5-S5-D5
S5-D5
D5-S5
S5
```

As you can see, S5 is the final card, its original position is position 1. Code produces this answer.