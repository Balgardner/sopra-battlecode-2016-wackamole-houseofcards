package sopra.battlecode.wackamole.houseofcards;

/**
 * It's all about a House of Cards and yet Kevin Spacey
 * isn't in it!!! :)
 */
public class App
{
  /**
   * Main method.
   *
   * @param args
   */
  public static void main(String[] args)
  {
    App app = new App();
    
    app.runFor(39);
    app.runFor(18);
    app.runFor(1);
    app.runFor(39);
    app.runFor(100000);
  }
  
  /**
   * Wrapper for the run method, prints nice words.
   *
   * @param numPacks
   */
  private void runFor(int numPacks)
  {
    System.out.println("Running application for: [" + numPacks + "] packs.."); 
    
    run(numPacks);
  }
  
  /**
   * Runs the app for a specified number 
   * of packs.
   *
   * @param numPacks
   */
  private void run(int numPacks)
  {
    Dealer.shuffle(Deck.build(numPacks));
  }
  
  /**
   * Swap this method in for the 'run' method in the 'runFor' method to see the composition of the deck. 
   * 
   * We believe the quiz-masters have provided incorrect data...
   * 
   * - Position 2009 of a/an 39 pack deck of cards corresponds to the 8D card, not the 7D card.
   * - Position  849 of a/an 18 pack deck of cards corresponds to the JH card, not the TH card.
   *
   * @param numPacks
   */
  private void printRun(int numPacks)
  {
    Deck.print(Deck.build(numPacks));
  }
}
