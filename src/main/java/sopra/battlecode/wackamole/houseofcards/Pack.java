/**
 * 
 */
package sopra.battlecode.wackamole.houseofcards;

import java.util.ArrayList;
import java.util.List;

/**
 * Pack understands how to build a single pack of cards.
 * 
 * All packs will be built according to instructions.
 * 
 * SPADES, HEARTS, DIAMONDS, CLUBS AKQJT98765432
 */
public class Pack
{
  /**
   * Builds a single pack of cards. 
   */
  public static List<Card> build(int initialIndex)
  {
    List<Card> cards = new ArrayList<Card>();
    int index        = initialIndex;
    
    for (String faceValue : Pack.getFaceValues())
    {
      cards.add(new Card(Suit.SPADES, faceValue, index));
      
      index++;
    }

    for (String faceValue : Pack.getFaceValues())
    {
      cards.add(new Card(Suit.HEARTS, faceValue, index));
      
      index++;
    }
    
    for (String faceValue : Pack.getFaceValues())
    {
      cards.add(new Card(Suit.DIAMONDS, faceValue, index));
      
      index++;
    }
    
    for (String faceValue : Pack.getFaceValues())
    {
      cards.add(new Card(Suit.CLUBS, faceValue, index));
      
      index++;
    }
    
    return cards;
  }
  
  public static List<String> getFaceValues()
  {
    List<String> faceValues = new ArrayList<String>();
    
    faceValues.add("A");
    faceValues.add("K");
    faceValues.add("Q");
    faceValues.add("J");
    faceValues.add("T");
    faceValues.add("9");
    faceValues.add("8");
    faceValues.add("7");
    faceValues.add("6");
    faceValues.add("5");
    faceValues.add("4");
    faceValues.add("3");
    faceValues.add("2");    

    return faceValues;
  }
}
