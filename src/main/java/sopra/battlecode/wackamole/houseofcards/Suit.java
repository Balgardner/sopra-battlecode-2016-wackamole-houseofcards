package sopra.battlecode.wackamole.houseofcards;

/**
 * Suit for a pack of cards. Can be one of...
 * 
 * - SPADES - DIAMONDS - HEARTS - CLUBS
 *
 */
public enum Suit
{
  SPADES("S"), DIAMONDS("D"), HEARTS("H"), CLUBS("C");

  private String value;

  private Suit(String value)
  {
    this.value = value;
  }
  
  public String value()
  {
    return this.value;
  }
}
