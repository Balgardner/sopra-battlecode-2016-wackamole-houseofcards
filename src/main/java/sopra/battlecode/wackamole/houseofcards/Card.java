package sopra.battlecode.wackamole.houseofcards;

/**
 * Represents one single card.
 * 
 * Keeps a record of...
 * 
 * - The suit the card belongs to.
 * - The value on the face of the card.
 * - The index position of the card in the initial deck. 
 *
 */
public class Card
{
  private Suit suit;
  private String value;
  private int initialIndex;
  
  public Card(Suit suit, String value, int initialIndex)
  {
    this.suit         = suit;
    this.value        = value;
    this.initialIndex = initialIndex;
  }
  
  public void print()
  {
    System.out.println(getValue() + getSuit().value() + getInitialIndex());
  } 
  
  public Suit getSuit()
  {
    return suit;
  }
  public void setSuit(Suit suit)
  {
    this.suit = suit;
  }
  public String getValue()
  {
    return value;
  }
  public void setValue(String value)
  {
    this.value = value;
  }
  public int getInitialIndex()
  {
    return initialIndex;
  }
  public void setInitialIndex(int initialIndex)
  {
    this.initialIndex = initialIndex;
  }
}
