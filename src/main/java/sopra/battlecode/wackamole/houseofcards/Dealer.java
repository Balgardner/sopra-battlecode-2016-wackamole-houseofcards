package sopra.battlecode.wackamole.houseofcards;

import java.util.List;

/**
 * Understands how to shuffle the cards.
 */
public class Dealer
{
  public static void shuffle(List<Card> cards)
  {
    Deck deck = new Deck(cards);
    
    System.out.println("Size of deck before pulling cards: " + deck.getCards().size());
    
    deck.pullCards();
    
    Card lastCard = deck.getCards().get(0);
    
    System.out.println("Size of deck after pulling cards: " + deck.getCards().size());
    
    lastCard.print();
    
    System.out.println("");
  }
}
