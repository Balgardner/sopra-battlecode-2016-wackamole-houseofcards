package sopra.battlecode.wackamole.houseofcards;

import java.util.ArrayList;
import java.util.List;

/**
 * Understands how to build a deck of cards comprising
 * an integer multiple number of packs of cards.
 */
public class Deck
{
  private List<Card> cards;
  private boolean even = true;
  
  /**
   * Initialising constructor.
   *
   * @param cards
   */
  public Deck(List<Card> cards)
  {
    this.cards = cards;
  }
  
  /**
   * Understands how to build a deck comprising {numPacks} number of packs of cards.
   *
   * @param numPacks
   * @return
   */
  public static List<Card> build(int numPacks)
  {
    List<Card> cards = new ArrayList<Card>();
    
    for (int i = 0; i < numPacks; i++)
    {
      int initialIndex = (52 * i) + 1;
      
      cards.addAll(Pack.build(initialIndex));
    }
    
    return cards;
  }
  
  public static void print(List<Card> cards)
  {
    for (Card card : cards)
    {
      card.print();
    }
  }
  
  /**
   * Understands how to pull a card from the deck.
   */
  public void pullCards()
  {
    while(this.cards.size() > 1)
    {
      Card topCard = this.cards.remove(0);
      
      if (this.even)
      {
        this.cards.add(topCard);
        
        this.even = false;
      }
      else
      {
        this.even = true;
      }      
    }
  }

  /**
   * Accessor for the named property.
   *
   * @return the cards
   */
  public List<Card> getCards()
  {
    return cards;
  }

  /**
   * Mutator for the named property.
   *
   * @param cards The value to set the named property to.
   */
  public void setCards(List<Card> cards)
  {
    this.cards = cards;
  }
}
